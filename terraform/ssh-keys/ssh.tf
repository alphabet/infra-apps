variable "ssh_keys_path" {
  type = string
}

output "ssh_keys" {
  value = [
    for fn in fileset(var.ssh_keys_path, "**") : file("${var.ssh_keys_path}/${fn}")
  ]
}
