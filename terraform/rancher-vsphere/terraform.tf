terraform {
  required_providers {
    rancher2 = {
      source  = "rancher/rancher2"
      version = "~> 1.11.0"
    }
    vsphere = {
      source  = "vsphere"
      version = "~> 1.24.3"
    }
  }
}
