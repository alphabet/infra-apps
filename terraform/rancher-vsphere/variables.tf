variable "vsphere_url" {
  type = string
}

variable "vsphere_unverified_ssl" {
  type    = bool
  default = false
}

variable "vsphere_datacenter" {
  type = string
}

variable "vsphere_cluster" {
  type = string
}

variable "vsphere_network" {
  type = string
}

variable "vm_datastore" {
  type = string
}

variable "vm_template" {
  type    = string
  default = "debian-10-vmware-cloud"
}

variable "vm_cloud_user" {
  type    = string
  default = "debian"
}

variable "vm_ssh_keys" {
  type = list(string)
}

variable "vm_name" {
  type    = string
  default = "rancher"
}

variable "vm_ip" {
  type = string
}

variable "vm_gateway" {
  type = string
}

variable "vm_dns" {
  type = string
}

variable "rancher_hostname" {
  type = string
}

variable "rancher_tls_noverify" {
  type    = bool
  default = false
}
