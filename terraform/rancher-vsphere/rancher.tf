locals {
  rancher = {
    "vm_name"    = "rancher"
    "address"    = split("/", var.vm_ip)[0]
    "netmask"    = split("/", var.vm_ip)[1]
    "gateway"    = var.vm_gateway
    "dns"        = var.vm_dns
    "ssh_keys"   = var.vm_ssh_keys
    "cloud_user" = var.vm_cloud_user
  }
}

resource "random_password" "admin_password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource "vsphere_virtual_machine" "rancher" {
  name             = local.rancher.vm_name
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id

  num_cpus = 2
  memory   = 4096
  guest_id = data.vsphere_virtual_machine.template.guest_id

  scsi_type = data.vsphere_virtual_machine.template.scsi_type

  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = data.vsphere_virtual_machine.template.network_interface_types[0]
  }

  disk {
    label            = "disk0"
    size             = "40"
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
  }

  extra_config = {
    "guestinfo.metadata" = base64encode(
      templatefile("${path.module}/../cloud-init/metadata.yaml",
        local.rancher
    ))
    "guestinfo.metadata.encoding" = "base64"
    "guestinfo.userdata" = base64encode(
      templatefile("${path.module}/../cloud-init/userdata.yaml",
        local.rancher
    ))
    "guestinfo.userdata.encoding" = "base64"
  }

  connection {
    type = "ssh"
    host = local.rancher.address
    user = local.rancher.cloud_user
  }

  provisioner "local-exec" {
    command = <<-EOT
      ansible-playbook ansible-playbook.yml \
      -i rancher, \
      -u ${local.rancher.cloud_user} -b \
      -e ansible_host=${local.rancher.address} \
      -e rancher_hostname=${var.rancher_hostname} \
      -e rancher_admin_password=${random_password.admin_password.result}
    EOT
    environment = {
      ANSIBLE_HOST_KEY_CHECKING = "False"
      ANSIBLE_FORCE_COLOR       = "true"
    }
  }
}

provider "rancher2" {
  api_url   = "https://${var.rancher_hostname}/"
  insecure  = var.rancher_tls_noverify
  bootstrap = true
}

resource "rancher2_bootstrap" "admin" {
  current_password = random_password.admin_password.result
  password         = random_password.admin_password.result
  telemetry        = true

  depends_on = [vsphere_virtual_machine.rancher]
}

output "admin_password" {
  value = random_password.admin_password.result
}

output "token" {
  value = rancher2_bootstrap.admin.token
}
