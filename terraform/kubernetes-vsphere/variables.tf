####################
# Global variables #
####################

variable "vsphere_url" {
  type = string
}

variable "vsphere_unverified_ssl" {
  type    = bool
  default = false
}

variable "vsphere_datacenter" {
  type = string
}

variable "vsphere_cluster" {
  type = string
}

variable "vsphere_network" {
  type = string
}

variable "vm_datastore" {
  type = string
}

variable "vm_template" {
  type    = string
  default = "debian-10-vmware-cloud"
}

variable "vm_cloud_user" {
  type    = string
  default = "debian"
}

variable "vm_name_prefix" {
  type    = string
  default = ""
}

variable "vm_name_suffix" {
  type    = string
  default = ""
}

variable "ssh_keys" {
  type = list(string)
}

variable "vm_remote_exec" {
  type    = list(string)
  default = []
}

variable "subnet_node" {
  type = string
}

variable "gateway_node" {
  type = string
}

variable "dns_node" {
  type    = string
  default = ""
}

###########################
# Control-plane variables #
###########################

variable "count_control_plane" {
  type = number
}

variable "offset_ip_control_plane" {
  type = number
}

variable "remote_exec_control_plane" {
  type    = list(string)
  default = []
}

####################
# Worker variables #
####################

variable "count_worker" {
  type = number
}

variable "offset_ip_worker" {
  type = number
}

variable "remote_exec_worker" {
  type    = list(string)
  default = []
}
