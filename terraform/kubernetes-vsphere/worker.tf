locals {
  workers = [
    for index in range(var.count_worker) :
    {
      "vm_name"     = "${var.vm_name_prefix}worker${var.vm_name_suffix}-${index + 1}"
      "address"     = cidrhost(var.subnet_node, var.offset_ip_worker + index),
      "netmask"     = split("/", var.subnet_node)[1],
      "gateway"     = var.gateway_node,
      "dns"         = var.dns_node,
      "ssh_keys"    = var.ssh_keys,
      "cloud_user"  = var.vm_cloud_user
      "remote_exec" = var.remote_exec_worker
    }
  ]
}

resource "vsphere_virtual_machine" "workers" {
  name             = each.key
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id

  num_cpus = 8
  memory   = 16384
  guest_id = data.vsphere_virtual_machine.template.guest_id

  scsi_type = data.vsphere_virtual_machine.template.scsi_type

  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = data.vsphere_virtual_machine.template.network_interface_types[0]
  }

  disk {
    label            = "disk0"
    size             = 40
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id

  }
  extra_config = {
    "guestinfo.metadata" = base64encode(
      templatefile("${path.module}/../cloud-init/metadata.yaml",
        each.value
    ))
    "guestinfo.metadata.encoding" = "base64"
    "guestinfo.userdata" = base64encode(
      templatefile("${path.module}/../cloud-init/userdata.yaml",
        each.value
    ))
    "guestinfo.userdata.encoding" = "base64"
  }

  connection {
    type = "ssh"
    host = each.value.address
    user = each.value.cloud_user
  }

  provisioner "remote-exec" {
    inline = each.value.remote_exec
  }

  for_each = { for worker in local.workers : worker.vm_name => worker }
}
