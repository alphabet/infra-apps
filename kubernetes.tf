terraform {
  backend "http" {}
  required_providers {
    rancher2 = {
      source  = "rancher/rancher2"
      version = "~> 1.11.0"
    }
  }
}

module "ssh_keys" {
  source        = "./terraform/ssh-keys"
  ssh_keys_path = "${path.module}/ssh-keys"
}

module "kubernetes" {
  source = "./terraform/kubernetes-vsphere"

  vsphere_network        = "PG_SNP-01_VLAN-4004"
  vsphere_url            = "vcentersnp01.in.apps.education.fr"
  vsphere_unverified_ssl = true
  vsphere_datacenter     = "DC_SNP_01"
  vsphere_cluster        = "CCL_SNP_01"
  vm_datastore           = "DATASTORE-SNP-01"
  subnet_node            = "172.29.32.64/26"
  gateway_node           = "172.29.32.126"
  dns_node               = "172.29.32.17"
  ssh_keys               = module.ssh_keys.ssh_keys
  vm_name_prefix         = "snp01-"

  offset_ip_control_plane = 8
  count_control_plane     = 3

  offset_ip_worker = 46
  count_worker     = 5

  remote_exec_control_plane = [
    "until sudo docker info; do sleep 5; done",
    "${rancher2_cluster.k8s_snp.cluster_registration_token[0].node_command} --etcd --controlplane --worker"
  ]
  remote_exec_worker = [
    "until sudo docker info; do sleep 5; done",
    "${rancher2_cluster.k8s_snp.cluster_registration_token[0].node_command} --worker"
  ]
}

module "rancher" {
  source = "./terraform/rancher-vsphere"

  vsphere_network        = "PG_SNP-01_VLAN-4004"
  vsphere_url            = "vcentersnp01.in.apps.education.fr"
  vsphere_unverified_ssl = true
  vsphere_datacenter     = "DC_SNP_01"
  vsphere_cluster        = "CCL_SNP_01"
  vm_datastore           = "DATASTORE-SNP-01"
  vm_ip                  = "172.29.32.90/26"
  vm_gateway             = "172.29.32.126"
  vm_dns                 = "172.29.32.17"
  vm_ssh_keys            = module.ssh_keys.ssh_keys
  rancher_hostname       = var.rancher_hostname
  rancher_tls_noverify   = var.rancher_tls_noverify
}

provider "rancher2" {
  api_url   = "https://${var.rancher_hostname}/"
  insecure  = var.rancher_tls_noverify
  token_key = module.rancher.token
}

resource "rancher2_cluster" "k8s_snp" {
  name        = "snp-prod"
  description = "SNP prod cluster"
  rke_config {
    network {
      plugin = "calico"
    }
    services {
      etcd {
        creation  = "6h"
        retention = "24h"
      }
      kube_api {
        audit_log {
          enabled = true
          configuration {
            max_age    = 5
            max_backup = 5
            max_size   = 100
            path       = "-"
            format     = "json"
            policy     = <<-EOT
              apiVersion: audit.k8s.io/v1
              kind: Policy
              metadata:
               creationTimestamp: null
              omitStages:
              - RequestReceived
              rules:
              - level: RequestResponse
                resources:
                - resources:
                  - pods
	   EOT
          }
        }
      }
    }
    upgrade_strategy {
      drain                  = true
      max_unavailable_worker = "20%"
    }
  }
}
output "rancher_url" {
  value = "https://${var.rancher_hostname}/"
}

output "rancher_admin_password" {
  value = module.rancher.admin_password
}
