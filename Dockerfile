FROM python:3.8 as base

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get -qq install curl git openssh-client sshpass sudo \
 && rm -rf /var/lib/apt/lists/*

FROM base as terraform-builder
ARG TERRAFORM_VERSION=0.14.5
RUN curl -sSLo terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
 && unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
 && install terraform /usr/local/bin \
 && rm -rf terraform_${TERRAFORM_VERSION}_linux_amd64.zip terraform

FROM base as kubectl-builder
# Install kubectl
ARG KUBECTL_VERSION=1.19.9
RUN curl -Lo /usr/local/bin/kubectl \
    https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
 && chmod +x /usr/local/bin/kubectl \
 && kubectl completion bash | install -Dm644 /dev/stdin /usr/share/bash-completion/completions/kubectl

FROM base as helm-builder
# Install Helm
ARG HELM_VERSION=3.5.1
RUN curl -sSL https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz | tar -xvz \
 && install linux-amd64/helm /usr/local/bin \
 && rm -rf linux-amd64 \
 && helm completion bash | install -Dm644 /dev/stdin /usr/share/bash-completion/completions/helm

FROM base
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    bash-completion \
 && rm -rf /var/lib/apt/lists/* \
 && echo 'source /usr/share/bash-completion/bash_completion' >> /etc/bash.bashrc

ARG ANSIBLE_VERSION=3.0.0
RUN pip install ansible==${ANSIBLE_VERSION}

COPY --from=terraform-builder /usr/local/bin/terraform /usr/local/bin
COPY --from=kubectl-builder /usr/local/bin/kubectl /usr/local/bin
COPY --from=kubectl-builder /usr/share/bash-completion/completions/kubectl /usr/share/bash-completion/completions/kubectl
COPY --from=helm-builder /usr/local/bin/helm /usr/local/bin
COPY --from=helm-builder /usr/share/bash-completion/completions/helm /usr/share/bash-completion/completions/helm

RUN echo 'enix ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers.d/eole
WORKDIR /workdir
ENTRYPOINT ["./scripts/detect-user.sh"]
CMD ["/bin/bash"]
