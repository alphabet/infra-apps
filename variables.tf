variable "terraform_state" {
  type    = string
  default = "main"
}

variable "rancher_hostname" {
  type    = string
  default = "rancher-master.in.apps.education.fr"
}

variable "rancher_tls_noverify" {
  type    = bool
  default = true
}
