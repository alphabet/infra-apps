# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/random" {
  version = "3.0.1"
  hashes = [
    "h1:SzM8nt2wzLMI28A3CWAtW25g3ZCm1O4xD0h3Ps/rU1U=",
    "zh:0d4f683868324af056a9eb2b06306feef7c202c88dbbe6a4ad7517146a22fb50",
    "zh:4824b3c7914b77d41dfe90f6f333c7ac9860afb83e2a344d91fbe46e5dfbec26",
    "zh:4b82e43712f3cf0d0cbc95b2cbcd409ba8f0dc7848fdfb7c13633c27468ed04a",
    "zh:78b3a2b860c3ebc973a794000015f5946eb59b82705d701d487475406b2612f1",
    "zh:88bc65197bd74ff408d147b32f0045372ae3a3f2a2fdd7f734f315d988c0e4a2",
    "zh:91bd3c9f625f177f3a5d641a64e54d4b4540cb071070ecda060a8261fb6eb2ef",
    "zh:a6818842b28d800f784e0c93284ff602b0c4022f407e4750da03f50b853a9a2c",
    "zh:c4a1a2b52abd05687e6cfded4a789dcd7b43e7a746e4d02dd1055370cf9a994d",
    "zh:cf65041bf12fc3bde709c1d267dbe94142bc05adcabc4feb17da3b12249132ac",
    "zh:e385e00e7425dda9d30b74ab4ffa4636f4b8eb23918c0b763f0ffab84ece0c5c",
  ]
}

provider "registry.terraform.io/hashicorp/vsphere" {
  version     = "1.24.3"
  constraints = "~> 1.24.3"
  hashes = [
    "h1:tmVOVBdsBWDIsPdIUgYguaA1dKAlUOKrnnmzGaMNAr4=",
    "zh:3b31455ca5a5c4ae7b65f1f4a51d651988d42862b9d4e078eb6fd5b238f98176",
    "zh:4935754d72299ecfd7081517f496274dee34fb021996266f1f1cc9a5c6b1129d",
    "zh:531dcccbc4f22cd09b19e217403d8545bc24ac291256ce3d2ec4484c400f7729",
    "zh:551da4dcd47b7736091e3a7e94c189be399043c7c3ec597e8edb9e6f7b2a2c1e",
    "zh:668db7ef747d8005194249288ebe081fa7f8be128081acc32915b2c769358391",
    "zh:b377f0d8a5340a3c661ee6e2a5c467f742f8f3679518375877cbcfd63b3804b1",
    "zh:c7573b9e2e2480f442704682f5dcf0756e78f890571e92dff08918262d0002cf",
    "zh:cd223874ba4acf6e6be98cc80c835ec4a172dbc0300c99b6a6ff8786a52dc092",
    "zh:dd523ff0fb2cfc9d6be2d14be1184cf9a90babc3f891630ee0f1e080ec87236e",
    "zh:e28991bff40a6c46d1a9f604b9bc9e6e6b59aa6bfb29b9ee7704a70ba262a332",
  ]
}

provider "registry.terraform.io/rancher/rancher2" {
  version     = "1.11.0"
  constraints = "~> 1.11.0"
  hashes = [
    "h1:4Duo5SQyArfvEB0CTrCzSiT5hdny6rTuuYDcfVezJNM=",
    "zh:1c7464f4a8711a92a06e1d5deb3f3785223f924bdf645b6ebe42d4d3783d0fee",
    "zh:716e543ae2e817bb48b80f0c10e014215b58f2521c731b5bdac0177886aa4150",
    "zh:748512912c686d4f1c578d9db890f091953cb5818eaec91cf49df58050e7b0d9",
    "zh:95bc35405b562ada9b33fc39ee6e1668842a59c74bce9b5912779b65c3a58936",
    "zh:98d4f138bfffdd0f73233250b22aefd7255bfac973d7bbd7677ee4decbe55ce2",
    "zh:9d2e543000268efcabec16d11cbe3a6e605d9d2587588aa630ec81133f5d460a",
    "zh:afa56b1e34cbfd9eb26e6ef8bda27a875974da7524c7ca2bb5522c0d397b4a23",
    "zh:b296cd8bfeabc53d42328b3b621406349dde765f7d64157cb6df64507b29c931",
    "zh:b40e9b1ccf2f2b00295565453761b1baa489a9a9bf362932aa16c9d5ec4e94a9",
    "zh:d4124ce4fea3537f6401441d2b2f49c1d59c1efad06654f7b68691831924ecf6",
    "zh:db56682e90228868e3ceba1f394ccf9e2b5fc5ae57b73ba953b37b2bb9adc258",
  ]
}
