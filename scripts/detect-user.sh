#! /bin/bash

set -e

PWD_UID=$(stat . --printf="%u")

ssh_agent(){
    if [ ! -S "${SSH_AUTH_SOCK}" ]; then
	. <(ssh-agent)
    fi
}

if [ "${PWD_UID}" -ne "0" ]; then
  getent passwd ${PWD_UID} || useradd -u ${PWD_UID} eole
  AGENT=$(declare -f ssh_agent)
  exec sudo -HEu eole -- $SHELL -c "$AGENT; ssh_agent; $@"
else
  ssh_agent
  $@
fi
