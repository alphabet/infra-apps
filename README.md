## Short Intro

Steps:
  - Install docker && docker-compose
  - Add you ssh public key in `./ssh-keys/`
  - run
     ```
     docker-compose run shell
     terraform init # Only the first time
     terraform apply # At each change
     ```

## Customize Shell

If you want to customize the shell environment to feel at home, or to load secrets directly;
create a `docker-compose.override.yml`

```
version: "3"
services:
  shell:
    env-file:
      - .env
    enviroment:
      FOO:
      BAR:
```
